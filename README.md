# apt

commandline package manager
* https://tracker.debian.org/pkg/apt

# Licence: GPLv2+
* https://salsa.debian.org/apt-team/apt/blob/master/COPYING
* https://sources.debian.org/src/apt/1.6.3/COPYING/

# Documentation used
## Suppressing "Reading database"
* https://askubuntu.com/questions/258219/how-do-i-make-apt-get-install-less-noisy

# Features
## Backports
* Demonstrated in https://gitlab.com/apt-packages-demo/flatpak/blob/master/.gitlab-ci.yml
  but have to be resumed and moved here.

## Message: "debconf: delaying package configuration, since apt-utils is not installed"
* https://www.google.com/search?q=debconf+delaying+package+configuration+since+apt-utils+is+not+installed
* https://github.com/phusion/baseimage-docker/issues/319
* https://github.com/baram204/Used-Car-Price-Expectation-with-ML/issues/8
* https://serverfault.com/questions/358943/what-does-debconf-delaying-package-configuration-since-apt-utils-is-not-insta
* https://dev.to/programliftoff/using-docker-for-experimenting-with-code-8i

# Other related tools
## tasksel (pkg and cmd)
* [Installing Software Collections After System Setup](https://www.answertopia.com/ubuntu/installing-ubuntu-with-the-network-installer/)
  2020-05 Answertopia
