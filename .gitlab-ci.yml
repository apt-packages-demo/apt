variables:
    APT_OUT: "/dev/null" # /dev/stdout # for debugging
    TERM: xterm-color
    TIMEFORMAT: "\n%0lR" # for bash time
    # DEBIAN_FRONTEND: noninteractive # TODO Check if this will not already be done in future Docker image!


image: debian:testing-slim

.header:
- set -o errexit -o nounset -o noglob -o pipefail
- shopt -s failglob
- (. /etc/os-release ; echo $PRETTY_NAME)

.apt-header:
- !reference [.header]
- ${SUDO:-} apt-get update --quiet=2
- ${SUDO:-} apt-get install
        --no-install-suggests
        --no-install-recommends
        --quiet=2
    moreutils
  &>> $APT_OUT
- export CHRONIC="chronic"


.install-chronic:
    script: exit
    before_script:
    - !reference [.apt-header]
    - time ${CHRONIC:-} ${SUDO:-} apt-get install
            --no-install-suggests
            --no-install-recommends
            --quiet=2
        hello
        tzdata

install-chronic-debian-testing:
    extends: .install-chronic
    image: debian:testing-slim

install-chronic-debian:
    extends: .install-chronic
    image: debian:stable-slim

install-chronic-kali-rolling:
    extends: .install-chronic
    image: kalilinux/kali-rolling

install-chronic-ubuntu-rolling:
    extends: .install-chronic
    variables:
        DEBIAN_FRONTEND: noninteractive
    image: ubuntu:rolling


.apt-header-utils:
- !reference [.header]
- ${SUDO:-} apt-get update --quiet=2
- ${SUDO:-} apt-get install
        --no-install-suggests
        --no-install-recommends
        --quiet=2
    apt-utils 
    moreutils
  &>> $APT_OUT
- export CHRONIC="chronic"

.install-utils:
    script: exit
    before_script:
    - !reference [.apt-header-utils]
    - time ${CHRONIC:-} ${SUDO:-} apt-get install
            --no-install-suggests
            --no-install-recommends
            --quiet=2
        hello
        tzdata

install-apt-utils:
    extends: .install-utils
    image: debian:testing-slim


.why-apt-utils: &why
  script:
  - set -o errexit -o nounset -o noglob -o pipefail
  - shopt -s failglob
  - (. /etc/os-release ; echo $PRETTY_NAME)

  - time ${SUDO:-} apt-get update --quiet &>> apt-get-1-update.txt
  - time ${SUDO:-} apt-get install
          --no-install-suggests
          --no-install-recommends
          --quiet=2
      hello
      tzdata
    &>> apt-get-2-install.txt
  - time ${SUDO:-} apt-get purge
          --quiet=2
      hello
      tzdata
    &>> apt-get-3-purge.txt
  - time ${SUDO:-} apt-get install
          --no-install-suggests
          --no-install-recommends
          --quiet=2
      apt-utils
    &>> apt-get-4-apt-utils.txt
  - time ${SUDO:-} apt-get install
          --no-install-suggests
          --no-install-recommends
          --quiet=2
      hello
      tzdata
    &>> apt-get-5-install.txt
  - (! diff apt-get-2-install.txt apt-get-5-install.txt --side-by-side > diff.txt)
  - (! diff apt-get-2-install.txt apt-get-5-install.txt)
  artifacts:
    paths:
    - apt-get-1-update.txt
    - apt-get-2-install.txt
    - apt-get-3-purge.txt
    - apt-get-4-apt-utils.txt
    - apt-get-5-install.txt
    - diff.txt

why-apt-utils:
  <<: *why

why-apt-utils-slim:
  <<: *why
  image: debian:stable-slim


.upgrade-apt-utils: &upgrade
  script:
  - set -o errexit -o nounset -o noglob -o pipefail
  - shopt -s failglob
  - (. /etc/os-release ; echo $PRETTY_NAME)

  - time ${SUDO:-} apt-get update --quiet &>> apt-get-1-update.txt
  - time ${SUDO:-} apt-get upgrade
      --with-new-pkgs
      --quiet=2
    &>> apt-get-2-upgrade.txt
  - time ${SUDO:-} apt-get install
          --no-install-suggests
          --no-install-recommends
          --quiet=2
      apt-utils
    &>> apt-get-3-install.txt
  - time ${SUDO:-} apt-get install
          --no-install-suggests
          --no-install-recommends
          --quiet=2
      hello
      tzdata
    &>> apt-get-3-install.txt
  artifacts:
    paths:
    - apt-get-1-update.txt
    - apt-get-2-upgrade.txt
    - apt-get-3-install.txt

upgrade-apt-utils:
  <<: *upgrade

upgrade-apt-utils-slim:
  <<: *upgrade
  image: debian:stable-slim

upgrade-apt-utils-testing-slim:
  <<: *upgrade
  image: debian:testing-slim
  # Upgrade may be faster on slim version and
  # there is more to upgrade on testing than on stable


backports-by-install:
  script:
  - set -o errexit -o nounset -o noglob -o pipefail
  - shopt -s failglob
  - (. /etc/os-release ; echo $PRETTY_NAME ; echo $VERSION_CODENAME)

  - VERSION_CODENAME=$(. /etc/os-release ; echo $VERSION_CODENAME)
  - echo "deb http://ftp.debian.org/debian ${VERSION_CODENAME}-backports main" 
  - echo "deb http://ftp.debian.org/debian ${VERSION_CODENAME}-backports main" | ${SUDO:-} sh -c "cat >> /etc/apt/sources.list"
  - time ${SUDO:-} apt-get update --quiet=2
  # - time ${CHRONIC:-} ${SUDO:-} apt-get upgrade --with-new-pkgs $YES_QUIET_APT_OPTS
  - time ${CHRONIC:-} ${SUDO:-} apt-get install
          --no-install-suggests
          --no-install-recommends
          --quiet=2
      apt-utils
  - ${CHRONIC:-} ${SUDO:-} apt-get install
         --target-release ${VERSION_CODENAME}-backports
         --no-install-suggests
         --no-install-recommends
         --quiet=2
      mandoc
  image: debian:stable-slim

backports-by-package:
  script:
  - set -o errexit -o nounset -o noglob -o pipefail
  - shopt -s failglob
  - (. /etc/os-release ; echo $PRETTY_NAME ; echo $VERSION_CODENAME)

  - VERSION_CODENAME=$(. /etc/os-release ; echo $VERSION_CODENAME)
  - echo "deb http://ftp.debian.org/debian ${VERSION_CODENAME}-backports main" 
  - echo "deb http://ftp.debian.org/debian ${VERSION_CODENAME}-backports main" | ${SUDO:-} sh -c "cat >> /etc/apt/sources.list"
  - time ${SUDO:-} apt-get update --quiet=2
  - time ${CHRONIC:-} ${SUDO:-} apt-get install
          --no-install-suggests
          --no-install-recommends
          --quiet=2
      apt-utils
      mandoc/${VERSION_CODENAME}-backports
    || true
  image: debian:stable-slim

# One could show that the name "stable-backports" is not working...

third-party-packages:
  allow_failure: true
  # Maybe dl.mercurylang.org is blocking connections from Gitlab or Gitlab is blocking connections to dl.mercurylang.org .
  # To use Mercury docker images are published on https://hub.docker.com/search?q=paulbone%2Fmercury  .
  script:
  - set -o errexit -o nounset -o noglob -o pipefail
  - shopt -s failglob
  - (. /etc/os-release ; echo $PRETTY_NAME)

  - VERSION_CODENAME=$(. /etc/os-release ; echo $VERSION_CODENAME) || (cat /etc/os-release ; exit 1)

  - time ${SUDO:-} apt-get update --quiet=2
  - time ${CHRONIC:-} ${SUDO:-} apt-get install
          --no-install-suggests
          --no-install-recommends
          --quiet=2
      ca-certificates
      curl
      gnupg
      iputils-ping
      mandoc
      tree
  - ping -c1 dl.mercurylang.org
  - time curl
          --no-progress-meter
      http://dl.mercurylang.org/deb/ | head
  - curl --silent https://paul.bone.id.au/paul.asc | ${SUDO:-} apt-key add -
  - echo "deb http://dl.mercurylang.org/deb/ $VERSION_CODENAME main" | ${SUDO:-} sh -c "cat > /etc/apt/sources.list.d/mercury.list"
  - echo "deb-src http://dl.mercurylang.org/deb/ $VERSION_CODENAME main" | ${SUDO:-} sh -c "cat >> /etc/apt/sources.list.d/mercury.list"
  - cat /etc/apt/sources.list.d/mercury.list
  - ${SUDO:-} apt-get update --quiet=2
  - ${CHRONIC:-} ${SUDO:-} apt-get install
          --no-install-suggests
          --no-install-recommends
          --quiet=2
      mercury-recommended
  - ${SUDO:-} makewhatis /usr/share/man
  - ${SUDO:-} makewhatis /usr/X11R6/man || true
  - ${SUDO:-} makewhatis /usr/local/man
  - apt-cache showsrc mercury
  - mmc --version
  image: debian:stable-slim
